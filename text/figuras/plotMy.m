
cory = sum(data(:,2:4),2);
oval = sum(data(:,5:7),2);
circ = sum(data(:,8:10),2);

green = sum(data(:,2:3:10),2);
red = sum(data(:,3:3:10),2);
gray = sum(data(:,4:3:10),2);

plot(1:125,cory(1:125),'LineWidth',1.5); hold on
plot(1:125,oval(1:125),'LineWidth',1.5);
plot(1:125,circ(1:125),'LineWidth',1.5); hold off

legend('Triangular','Bastonete','Circular');
ylabel('Quantidade')
xlabel('Iteração')

figure

plot(1:125,green(1:125),'LineWidth',1.5,'Color','green'); hold on
plot(1:125,red(1:125),'LineWidth',1.5,'Color','red');
plot(1:125,gray(1:125),'LineWidth',1.5,'Color',[0.6 0.6 0.6]); hold off

legend('Verde','Vermelho','Cinza');
ylabel('Quantidade')
xlabel('Iteração')