
package bachome;

import bachome.GA.Cromossome;
import bachome.GA.GA;
import bachome.GA.Population;
import bachome.GA.implementations.ArithmeticCrossover;
import bachome.GA.implementations.GaussianMutation;
import bachome.GA.implementations.GerationalSelection;
import bachome.GA.implementations.MediumPressureFitness;
import bachome.GA.implementations.MediumPressureSelection;
import bachome.GA.implementations.OnePointCrossover;
import bachome.GA.implementations.TournamentSelection;
import bachome.GA.implementations.UniformMutation;
import bachome.GA.implementations.UserCrossover;
import bachome.GA.implementations.UserFitness;
import bachome.GA.implementations.UserMutation;
import bachome.GA.implementations.UserParentSelection;
import bachome.GA.implementations.UserSurvivorSelection;
import java.util.ArrayList;
import java.util.List;


public class Controller {
    
    private static GA ga;
    private static Inputs inputs;
    
    public static void main(String[] args) throws InterruptedException  {
        
        UI ui = new UI();
        inputs = ui.getInputs();
        
        if(inputs.getRepresentation().equals("Gerational"))
            ga = new GA(inputs.getSizePop(), 2, inputs.getMaxGen(),"rr");
        else
            ga = new GA(2, 2, 4000,"00");
        
        //First gene: color
        //Second gene: shape
        
	while (true) {
            
            long time = System.currentTimeMillis();
            
            ui.repaint();
            
            if(inputs.changeFlag()){
                updateGAConfig();
            }
            
            update();
            ui.setInputs(inputs);
            
            int fps = inputs.getSpeed();
            time = (2000/fps)-(System.currentTimeMillis()-time); 
            if (time > 0){ 
                try {
                    Thread.sleep(time);
                }catch(InterruptedException e) {
                    System.out.println("Sleeping error");
                }
            }
	}
    }
    
    public static void Controller(){
        
    }
    
    
    public static void updateGAConfig(){
        
        //TO-DO:
        //Adicionar troca de operadores
        
        double crossoverRate = inputs.getCrossProb();
        //Need to find find a better way do calculate the mutation probability
        //It's a little noxious keep changing the AG's basic structure
        double mutationProb = inputs.getMutProb();
        
        ga.setProbabilities(crossoverRate, mutationProb);
        
        //What is allowed to mutate.
        String shape = "00";
        if(inputs.doMut_color())
            shape = "r0";
        if(inputs.doMut_shape())
            shape = "0r";
        if(inputs.doMut_color() && inputs.doMut_shape())
            shape = "rr";
        ga.setCromossomeShape(shape);
        
        //Operators
        switch(inputs.getFitness()){
            case "Natural":
                ga.setFitness(new MediumPressureFitness());
                break;
            case "Usuário":
                ga.setFitness(new UserFitness());
                break;
            default:
                System.out.println("Fitness operator not recognized");
        }
        switch(inputs.getParentSelection()){
            case "Roleta":
                //TO-DO
                //Really? I forgot to implement this one
                //ga.setParentSelection(new WheelSelection());
                break;
            case "Torneio":
                ga.setParentSelection(new TournamentSelection());
                break;
            case "Usuário":
                ga.setParentSelection(new UserParentSelection());
                break;
            default:
                System.out.println("Parent Selection operator not recognized");
        }
        switch(inputs.getCrossover()){
            case "Um Ponto":
                ga.setCrossover(new OnePointCrossover());
                break;
            case "Múltiplos Pontos":
                //TO-DO
                //I was to lazy to do this one
                //ga.setCrossover(new MultiPointCrossover());
                break;
            case "Aritmético":
                ga.setCrossover(new ArithmeticCrossover());
                break;
            case "Usuário":
                ga.setCrossover(new UserCrossover());
                break;
            default:
                System.out.println("Crossover operator not recognized");
        }
        switch(inputs.getMutation()){
            case "Inversão":
                //TO-DO
                //Didn't implement this one
                //ga.setMutation(new FlipMutation());
                break;
            case "Uniforme":
                ga.setMutation(new UniformMutation());
                break;
            case "Gaussiana":
                ga.setMutation(new GaussianMutation());
                break;
            case "Usuário":
                ga.setMutation(new UserMutation());
                break;
            default:
                System.out.println("Crossover operator not recognized");
        }
        switch(inputs.getSurvivorSelection()){
            case "Geracional":
                ga.setSurvivorSelection(new GerationalSelection());
                break;
            case "Natural":
                ga.setSurvivorSelection(new MediumPressureSelection());
                break;
            case "Usuário":
                ga.setSurvivorSelection(new UserSurvivorSelection());
                break;
            default:
                System.out.println("Crossover operator not recognized");
        }
        
        //Simulation speed
    }
    
    public static void update(){
        ga.iterate(inputs);
        Population pop = ga.getPopulation();
        List<VisualElement> velList = new ArrayList<>();
        
        int numEl[] = new int[9];
        int t1 = 0;
        int t2 = 0;
        //pop.printPopulation();
        for(int i = 0; i < pop.getNIndividuals(); i++){
            
            Cromossome crm = pop.getIndividual(i).getCromossome();
            String type = "";
            
            if(crm.getGene(1).getValue() <= 0.33){
                 type += "Circ";
                 t1 = 0;
            }else if(crm.getGene(1).getValue() <= 0.66){
                 type += "Oval";
                 t1 = 1;
            }else if(crm.getGene(1).getValue() <= 1.00){
                 type += "Cory";
                 t1 = 2;
            }
            
            if(crm.getGene(0).getValue() <= 0.33){
                type += "Gray";
                t2 = 0;
            }else if(crm.getGene(0).getValue() <= 0.66){
                type += "Red";
                t2 = 1;
            }else if(crm.getGene(0).getValue() <= 1.00){
                type += "Green";
                t2 = 2;
            }
            
            if(type.length() > 10)
                System.out.println(type);
            
            
            VisualElement vel = new VisualElement(type);
            vel.setId(pop.getIndividual(i).getID());
            velList.add(vel);
            
            type = "";
            
            numEl[t1*3+t2]++;
        }
        
        //Prints population's distribution
        //     Circ              Oval               Cory
        //Grey Red Green    Grey Red Green     Grey Red Green
        /*System.out.print("\n" + ga.getGeneration());
        for(int i = 0; i < 9; i++)
            System.out.print(" " + numEl[i]);*/
        
        inputs.setElements(velList);
    }
}
  