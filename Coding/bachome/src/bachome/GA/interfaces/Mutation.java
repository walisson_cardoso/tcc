
package bachome.GA.interfaces;

import bachome.GA.Individual;


public interface Mutation {
    public Individual doMutation(Individual ind, String shape);
    public void       setProbability(double probability);
    public double     getProbability();
}
