
package bachome.GA.interfaces;

import bachome.GA.Population;

public interface SurvivorSelection {
    public Population doSelection(Population pop, Population nextPop);
}
