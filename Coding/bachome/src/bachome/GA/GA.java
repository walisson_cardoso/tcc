
package bachome.GA;

import bachome.GA.implementations.MediumPressureSelection;
import bachome.GA.implementations.TournamentSelection;
import bachome.GA.implementations.MediumPressureFitness;
import bachome.GA.implementations.OnePointCrossover;
import bachome.GA.implementations.UniformMutation;
import bachome.GA.implementations.UserFitness;
import bachome.GA.interfaces.SurvivorSelection;
import bachome.GA.interfaces.ParentSelection;
import bachome.GA.interfaces.Mutation;
import bachome.GA.interfaces.Fitness;
import bachome.GA.interfaces.Crossover;
import bachome.Inputs;
import javax.swing.JOptionPane;

public class GA {
    
    private int generation;
    private int maxGen;
    private boolean firstCalc = true;
    
    private Population population;
    private Population nextPopulation;
    private Individual best;
    
    private Fitness           fitness;
    private ParentSelection   parentSelection;
    private Crossover         crossover;
    private Mutation          mutation;
    private SurvivorSelection survivorSelection;
    
    private String cromossomeShape;
    
    public GA(int nInd, int nGenes, int maxGen, String cromossomeShape){
        if(maxGen > 0)
            this.maxGen = maxGen;
        else
            System.out.println("GA: invalid maximum of generations");
        
        population      = new Population(nInd, nGenes);
        
        fitness           = new MediumPressureFitness();
        parentSelection   = new TournamentSelection();
        crossover         = new OnePointCrossover();
        mutation          = new UniformMutation();
        survivorSelection = new MediumPressureSelection();
        
        population.initPopulation(cromossomeShape);
        best = new Individual(nGenes);
        
        this.cromossomeShape = cromossomeShape;
    }
    
    public void iterate(Inputs inputs){
        
        if(firstCalc){
            firstCalc = false;
            population = updateFitness(population, inputs);
        }
        
        int nInds = population.getNIndividuals();
        nextPopulation = new Population(nInds, population.getNGenes());
        GAUse.setCurrentPopSize(nInds);
        
        for(int i = 0; i < nInds; i++){
            Individual par1 = parentSelection.select(population);
            Individual par2 = parentSelection.select(population);
            
            Individual son = crossover.doCrossover(par1, par2);
                       son = mutation.doMutation(son, cromossomeShape);
            nextPopulation.setIndividual(son, i);
        }
        
        if(nInds == 0){
            JOptionPane.showMessageDialog(null, "A população Morreu!");
            System.exit(0);
        }
        
        population = survivorSelection.doSelection(population, nextPopulation);
        
        population = updateFitness(population, inputs);
        
        Cromossome crm = best.getCromossome();
        
        population.increaseLifeTime();
        
        generation++;
    }
    
    private Population updateFitness(Population population, Inputs inputs){
        int nInd = population.getNIndividuals();
        for(int i = 0; i < nInd; i++){
            Individual ind = population.getIndividual(i);
            double fit = fitness.evaluate(ind, inputs);
            ind.setFitness(fit);
            if(ind.getFitness() > best.getFitness())
                best.copy(ind);
            
            population.setIndividual(ind, i);
        }
        
        if(GAUse.elitism())
            population.setIndividual(best, 0);
        
        return population;
    }
    
    public void setProbabilities(double probCross, double probMut){
        //TO-DO write this function
        crossover.setProbability(probCross);
        mutation.setProbability(probMut);
    }
    
    public void setFitness(Fitness fit){
        this.fitness = fit;
    }
    
    public void setParentSelection(ParentSelection parSel){
        this.parentSelection = parSel;
    }
    
    public void setCrossover(Crossover cross){
        this.crossover = cross;
    }
    
    public void setMutation(Mutation mut){
        this.mutation = mut;
    }
    
    public void setSurvivorSelection(SurvivorSelection surSel){
        this.survivorSelection = surSel;
    }
    
    public void setCromossomeShape(String shape){
        this.cromossomeShape = shape;
    }
    
    public Population getPopulation(){
        return population;
    }
    
    public Individual getBestIndividual(){
        return best;
    }
    
    public int getGeneration(){
        return generation;
    }
    
    public boolean hasFinished(){
        
        return (generation >= maxGen);
    }
}
