
package bachome.GA;

public class Population {
    private int nIndividuals;
    private Individual individuals[];
    
    public Population(){
        this.nIndividuals = 0;
        this.individuals  = new Individual[0];
    }
    
    public Population(int nIndividuals, int nGenes){
        if(nIndividuals > 0){
            this.nIndividuals = nIndividuals;
            this.individuals = new Individual[nIndividuals];
            for(int i = 0; i < nIndividuals; i++)
                this.individuals[i] = new Individual(nGenes);
        }else{
            System.out.println("Population: Invalid number of individuals: " + nIndividuals);
        }
    }
    
    public void initPopulation(){
        for(int i = 0; i < nIndividuals; i++){
            individuals[i].initIndividual();
            individuals[i].setID(GAUse.assignID());
        }
    }
    
    public void initPopulation(String shape){
        for(int i = 0; i < nIndividuals; i++){
            individuals[i].initIndividual(shape);
            individuals[i].setID(GAUse.assignID());
        }
    }
    
    public void increaseLifeTime(){
        for(int i = 0; i < nIndividuals; i++)
            individuals[i].increaseTime();
    }
    
    public void setIndividual(Individual ind, int index){
        if(index >=0 && index < individuals.length)
            individuals[index] = ind;
        else
            System.out.println("setIndividual: invalid index");
    }
    
    public Individual getIndividual(int index){
        if(index >=0 && index < individuals.length)
            return individuals[index];
        else{
            System.out.println("getIndividual: invalid index");
            return null;
        }
    }
    
    public int getNIndividuals(){
        return nIndividuals;
    }
    
    public int getNGenes(){
        if(nIndividuals > 0)
            return individuals[0].getCromossome().getNGenes();
        else
            return 0;
    }
    
    public void printPopulation(){
        System.out.println("Number of individuals: " + nIndividuals);
        for(int i = 0; i < nIndividuals; i++){
            individuals[i].printIndivídual();
        }
    }
}
