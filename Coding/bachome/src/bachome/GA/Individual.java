
package bachome.GA;

import bachome.RandomPool;

public class Individual {
    private double fitness;
    private long ID;
    private long parentsID[];
    private int lifeTime;
    private Cromossome cromossome;
    
    public  Individual(){
        
        this.fitness = RandomPool.r().nextDouble()*0.1;
    }
    
    public Individual(int nGenes){
        this.cromossome = new Cromossome(nGenes);
        this.parentsID  = new long[1];
        this.lifeTime   = 0;
        this.fitness = RandomPool.r().nextDouble()*0.1;
    }
    
    public void initIndividual(){
        this.cromossome.initGenes();
        this.fitness = RandomPool.r().nextDouble()*0.1;
    }
    
    public void initIndividual(String shape){
        this.cromossome.initGenes(shape);
        this.fitness = RandomPool.r().nextDouble()*0.1;
    }
    
    public void copy(Individual ind){
        //A terrible copy function because java copies references all time
        this.fitness    = ind.fitness;
        this.ID         = ind.ID;
        this.lifeTime   = ind.lifeTime;
        
        int nGenes = ind.getCromossome().getNGenes();
        Cromossome crm = new Cromossome(nGenes);
        for(int i = 0; i < nGenes; i++){
            Gene gen = new Gene(ind.getCromossome().getGene(i).getValue());
            crm.setGene(gen, i);
        }
        this.cromossome = crm;
        
        this.parentsID  = new long[ind.getParentsID().length];
        long ids[] = ind.getParentsID();
        for(int i = 0; i < this.parentsID.length; i++)
            this.parentsID[i] = ids[i];
    }
    
    public void setCromossome(Cromossome cromossome){
        this.cromossome = cromossome;
    }
    
    public Cromossome getCromossome(){
        return cromossome;
    }
    
    public void setFitness(double fitness){
        this.fitness = fitness;
    }
    
    public double getFitness(){
        return fitness;
    }
    
    public void setID(long ID){
        this.ID = ID;
    }
    
    public long getID(){
        return ID;
    }
    
    public void setParentsID(long IDs[]){
        this.parentsID = IDs;
    }
    
    public long[] getParentsID(){
        return parentsID;
    }
    
    public void setLifeTime(int life){
        if(life >= 0)
            lifeTime = life;
        else
            System.out.println("setLifeTime: invalid time to live");
    }
    
    public void increaseTime(){
        lifeTime++;
    }
    
    public int getLifeTime(){
        return lifeTime;
    }
    
    public void printIndivídual(){
        System.out.printf("Fitness: %1.4f\n",fitness);
        for(int i = 0; i < getCromossome().getNGenes(); i++)
            System.out.printf("%1.3f ", cromossome.getGene(i).getValue());
        System.out.println("");
    }
}
