
package bachome.GA.implementations;

import bachome.GA.Population;
import bachome.GA.interfaces.SurvivorSelection;

public class GerationalSelection implements SurvivorSelection{
    
    
    @Override
    public Population doSelection(Population pop, Population nextPop){
        int nInds = pop.getNIndividuals();
        
        for(int i = 0; i < nInds; i++){
            pop.setIndividual(nextPop.getIndividual(i), i);
        }
        
        return pop;
    }
}
