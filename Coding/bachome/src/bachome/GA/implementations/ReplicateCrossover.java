
package bachome.GA.implementations;

import bachome.GA.Individual;
import bachome.GA.interfaces.Crossover;

public class ReplicateCrossover implements Crossover{
    
    private double probability;
    
    @Override
    public Individual doCrossover(Individual parent1, Individual parNull){
        long IDs[] = new long[1];
        IDs[0] = parent1.getID();
        
        Individual son = new Individual();
        son.setLifeTime(0);
        son.copy(son);
        son.setParentsID(IDs);
        
        return son;
    }
    
    @Override
    public void setProbability(double probability){
        
        if(probability >= 0 && probability <= 1)
            this.probability = probability;
        else
            System.out.println("setProbability: invalid probability");
    }
    
    @Override
    public double getProbability(){
        
        return this.probability;
    }
}
