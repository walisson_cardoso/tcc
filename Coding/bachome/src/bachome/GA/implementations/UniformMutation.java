
package bachome.GA.implementations;

import bachome.GA.Cromossome;
import bachome.GA.Gene;
import bachome.GA.Individual;
import bachome.GA.interfaces.Mutation;
import bachome.RandomPool;


public class UniformMutation implements Mutation{
    
    private double probability;
    
    @Override
    public Individual doMutation(Individual ind, String shape){
        
        if(RandomPool.r().nextDouble() < probability){
            
            Cromossome crm = ind.getCromossome();
            int position   = RandomPool.r().nextInt(crm.getNGenes());
            Gene gene      = crm.getGene(position);
            
            double value = 0;
            if(shape.charAt(position) == 'r'){
                value = RandomPool.r().nextDouble(true,true);
            }
            
            gene.setValue(value);
            crm.setGene(gene, position);
            ind.setCromossome(crm);
        }
        
        return ind;
    }
    
    @Override
    public void setProbability(double probability){
        
        if(probability >= 0 && probability <= 1)
            this.probability = probability;
        else
            System.out.println("setProbability: invalid probability");
    }
    
    @Override
    public double getProbability(){
        
        return this.probability;
    }
}
