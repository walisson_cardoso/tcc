
package bachome.GA.implementations;

import bachome.GA.Cromossome;
import bachome.GA.Gene;
import bachome.GA.Individual;
import bachome.GA.GAUse;
import bachome.GA.interfaces.Crossover;
import bachome.RandomPool;

public class OnePointCrossover implements Crossover{
    
    private double probability;
    
    @Override
    public Individual doCrossover(Individual parent1, Individual parent2){
        
        Individual son = new Individual();
        son.copy(parent1);
        
        long IDs[] = new long[2];
        IDs[0] = parent1.getID();
        IDs[1] = parent2.getID();
        
        Cromossome crm1  = son.getCromossome();
        Cromossome crm2 = parent2.getCromossome();
        
        int nGenes = crm1.getNGenes();
        int cutPoint = RandomPool.r().nextInt(nGenes);
        for(int i = cutPoint; i < nGenes; i++){
            Gene g = crm2.getGene(i);
            crm1.setGene(g, i);
        }
        
        son.setLifeTime(0);
        son.setCromossome(crm1);
        son.setParentsID(IDs);
        son.setID(GAUse.assignID());
        
        return son;
    }
    
    @Override
    public void setProbability(double probability){
        
        if(probability >= 0 && probability <= 1)
            this.probability = probability;
        else
            System.out.println("setProbability: invalid probability");
    }
    
    @Override
    public double getProbability(){
        
        return this.probability;
    }
}
