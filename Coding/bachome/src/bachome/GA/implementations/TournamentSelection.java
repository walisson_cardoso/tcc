
package bachome.GA.implementations;

import bachome.GA.Individual;
import bachome.GA.Population;
import bachome.GA.interfaces.ParentSelection;
import bachome.RandomPool;

public class TournamentSelection implements ParentSelection{
    
    @Override
    public Individual select(Population pop){
        int ringSize = 3;
        
        int popSize = pop.getNIndividuals();
        int fighters[] = new int[ringSize];
        for(int i = 0; i < ringSize; i++)
            fighters[i] = RandomPool.r().nextInt(popSize);
        
        int selected = 0;
        for(int i = 0; i < ringSize; i++)
            if(pop.getIndividual(fighters[i]).getFitness() > pop.getIndividual(fighters[selected]).getFitness())
                selected = i;
        
        Individual champion = new Individual();
        champion.copy(pop.getIndividual(fighters[selected]));
        
        return champion;
    }
}
