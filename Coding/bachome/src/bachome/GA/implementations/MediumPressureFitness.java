
package bachome.GA.implementations;

import bachome.GA.Cromossome;
import bachome.GA.GAUse;
import bachome.GA.Individual;
import bachome.GA.interfaces.Fitness;
import bachome.Inputs;
import bachome.RandomPool;


public class MediumPressureFitness implements Fitness{
    
    @Override
    public double evaluate(Individual ind, Inputs inputs){
        
        double fitness = 0;
        
        Cromossome crm = ind.getCromossome();
        
        /*
        Color:
            [0.00,0.33] --> branca
            ]0.33,0.66] --> vermelha
            ]0.66,1.00] --> verde
        
        Shape:
            [0.00,0.33] --> redonda
            ]0.33,0.66] --> com flagelo
            ]0.66,1.00] --> grande
        
            //Bacs verdes são melhores com antibio
            //Bacs grandes comem melhor
            //Bacs com fragelo fogem de predadores.
            //Bacs vemelhas são melhores com temperatura
            //Bacs brancas são melhores com PH
        
        */
        
        if(inputs.doMut_color()){
            if(crm.getGene(0).getValue() <= 0.33 && inputs.isApply_ph())
                fitness += crm.getGene(0).getValue()* Math.abs(inputs.getPh_press()/10.0 - 0.5);
            else if(crm.getGene(0).getValue() <= 0.66 && inputs.isApply_temper())
                fitness += (crm.getGene(0).getValue()-0.33)* Math.abs(inputs.getTemper_press()/10.0 - 0.5);
            else if(crm.getGene(0).getValue() <= 1.00 && inputs.isApply_antibio())
                fitness += (crm.getGene(0).getValue()-0.66)* (inputs.getAntibio_press()/10.0);
        }
            
        if(inputs.doMut_shape()){
           if(crm.getGene(1).getValue() <= 0.33)
               fitness += 0;
           else if(crm.getGene(1).getValue() <= 0.66 && inputs.isSelec_pred())
               fitness += (crm.getGene(1).getValue()-0.33)*(inputs.getSelec_pred_press()/10.0);
           else if(crm.getGene(1).getValue() <= 1.00 && inputs.isSelec_food())
               fitness += (crm.getGene(1).getValue()-0.66)*(inputs.getSelec_food_press()/10.0);
        }
        
        
        fitness += RandomPool.r().nextDouble()*0.01;
                
        return fitness;
    }
}
