
package bachome;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;


public class Sprite{
    
    private int X = 0;
    private int Y = 0;
    private int HEIGHT = 0;
    private int WIDTH = 0;
    
    BufferedImage image;
    BufferedImage origImage;
    
    public Sprite(){
        
    }

    public Sprite(int X, int Y) {
        this.X = X;
        this.Y = Y;
    }
    
    public Sprite(int X, int Y, String name) {
        this.X = X;
        this.Y = Y;
        
        load(name);
    }
    
    public void load(String name){
        try {
            this.origImage = ImageIO.read(getClass().getResource(name));
            this.WIDTH = this.origImage.getWidth();
            this.HEIGHT = this.origImage.getHeight();
            this.image = this.origImage;
        } catch (Exception e) {
            System.err.println("Didn't find image: " + name);
        }
    }
    
    public void paint(Graphics2D g) {
        g.drawImage(image, X, Y, null);
    }
    
    public boolean mouseClicked(MouseEvent me) {
        Point p = me.getPoint();
	if (p.getX() >= X && p.getY() >= Y && p.getX() <= X+WIDTH && p.getY() <= Y+HEIGHT)
            return true;
        else
            return false;
            
    }
    
    public void resize(int newWidth, int newHeight){
        
        double wProp = ((double) newWidth) / origImage.getWidth();
        double hProp = ((double) newHeight) / origImage.getHeight();
        
        AffineTransform tx = AffineTransform.getScaleInstance(wProp, hProp);
        AffineTransformOp op_pointer = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
        image = op_pointer.filter(origImage, null);
        
        
        this.WIDTH = newWidth;
        this.HEIGHT = newHeight;
    }
    
    public void setPos(int x, int y){
        this.X = x;
        this.Y = y;
    }
    
    public int getX(){
        return X;
    }
    
    public int getY(){
        return Y;
    }
    
    public int getWidth(){
        return WIDTH;
    }
    
    public int getHeight(){
        return HEIGHT;
    }
    
}
