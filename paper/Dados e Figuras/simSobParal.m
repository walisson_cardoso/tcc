
begin = 20;
tick = 40;

%% Load Data
data = xlsread('simula��o sobreviventes paralelo.xlsx');

index = data(:,1);
green = sum(data(:,2:3:10),2);
red = sum(data(:,3:3:10),2);
gray = sum(data(:,4:3:10),2);

age = data(:,11);

%% Plot Data

hFig = figure;
set(hFig, 'Position', [400 200 700 350]);
plot(index,green(index),'LineWidth',1.5,'Color',[0.5 0.5 0.5]); hold on
plot(index,red(index),'LineWidth',1.5,'Color','red');
plot(index,gray(index),'LineWidth',1.5,'Color',[0.0 0.6 0.57]); hold off

l2 = legend('Cinza','Vermelho','Azul');
ylabel('Quantidade','FontSize',14);
xlabel('Itera��o','FontSize',14);
minx = index(1);
maxx = index(end);
miny = 0;
maxy = max(max(data(:,2:end)))+10;

axis([minx, maxx, miny, maxy]);
x = index(begin:tick:end-1);
y = ones(length(x),1)*maxy;
[xa1 ya1] = ds2nfu(x, y);
set(gca,'XTick',[]);

minx = index(1);
maxx = index(end);
miny = 0;
maxy = max(max(data(:,2:end)))+10;

axis([minx, maxx, miny, maxy]);
x = index(begin:tick:end-1);
y = zeros(length(x),1);
[xa2 ya2] = ds2nfu(x, y);
set(gca,'XTick',x);


%% Draw Lines

for k=1:length(x)
	annotation('line',[xa1(k) xa2(k)],[ya1(k) ya2(k)],'LineStyle','--','color','k');
    
    dim = [xa1(k)-0.011 ya1(k)-0.22 .3 .3];
    str = ['t_' num2str(k)];
    an = annotation('textbox',dim,'String',str,'FitBoxToText','on');
    set(an,'EdgeColor','none');
end

uistack(l2,'top')
