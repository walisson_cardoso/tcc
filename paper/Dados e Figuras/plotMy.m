%% Load Data
index = data(:,1);
cory = sum(data(:,2:4),2);
oval = sum(data(:,5:7),2);
circ = sum(data(:,8:10),2);

green = sum(data(:,2:3:10),2);
red = sum(data(:,3:3:10),2);
gray = sum(data(:,4:3:10),2);

begin = 10;
tick = 30;

%% Plot Data

hFig = figure;
set(hFig, 'Position', [400 200 700 700]);

subplot(2,1,1);
plot(index,cory(index),'black','LineWidth',1.5); hold on
plot(index,oval(index),'red','LineWidth',1.5);
plot(index,circ(index),'blue','LineWidth',1.5); hold off

l1 = legend('Circular','Alongado','Triangular');
ylabel('Quantidade','FontSize',14);
xlabel('Itera��o','FontSize',14);

minx = index(1);
maxx = index(end);
miny = 0;
maxy = max(max(data(:,2:end)))+10;

axis([minx, maxx, miny, maxy]);
x = index(begin:tick:end-1);
y = ones(8,1)*maxy+2;
[xa1 ya1] = ds2nfu(x, y);
set(gca,'XTick',[]);

subplot(2,1,2);
plot(index,green(index),'LineWidth',1.5,'Color',[0.5 0.5 0.5]); hold on
plot(index,red(index),'LineWidth',1.5,'Color','red');
plot(index,gray(index),'LineWidth',1.5,'Color',[0.0 0.6 0.57]); hold off

l2 = legend('Cinza','Vermelho','Azul');
ylabel('Quantidade','FontSize',14);
xlabel('Itera��o','FontSize',14);
minx = index(1);
maxx = index(end);
miny = 0;
maxy = max(max(data(:,2:end)))+10;

axis([minx, maxx, miny, maxy]);
x = index(begin:tick:end-1);
y = zeros(8,1);
[xa2 ya2] = ds2nfu(x, y);
set(gca,'XTick',x);


%% Draw Lines

for k=1:8
	annotation('line',[xa1(k) xa2(k)],[ya1(k) ya2(k)],'LineStyle','--','color','k');
    
    dim = [xa1(k)-0.011 ya1(k)-0.26 .3 .3];
    str = ['t_' num2str(k)];
    an = annotation('textbox',dim,'String',str,'FitBoxToText','on');
    set(an,'EdgeColor','none');
end

%% Draw Letters

dim = [0.033 0.63 .3 .3];
str = 'a)';
an = annotation('textbox',dim,'String',str,'FitBoxToText','on');
set(an,'FontSize',14);
set(an,'EdgeColor','none');

dim = [0.033 0.16 .3 .3];
str = 'b)';
an = annotation('textbox',dim,'String',str,'FitBoxToText','on');
set(an,'FontSize',14);
set(an,'EdgeColor','none');

uistack(l1,'top')
uistack(l2,'top')
